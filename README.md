## Overview
Provide a [Colissimo](https://www.laposte.fr/colissimo) shipping method for [commerce_shipping](https://www.drupal.org/project/commerce_shipping).

## Features
- Supports home delivery with or without signatures, and relay pickup points.
- For relay pickup, displays a map with pickup points, centered on the user's
geolocation.
- Label generation
- Tracking link

## Requirements
- [Commerce Shipping 2.x](https://www.drupal.org/project/commerce_shipping)
- [Commerce Shipping Label](https://www.drupal.org/project/commerce_shipping_label)

## Configuration

Go to Commerce => Configuration => Shipping => Colissimo Settings.

You need to fill in your Colissimo credentials, and optionally adjust Label
generation settings.

## Similar projects

- [Commerce Colissimo](https://www.drupal.org/project/commerce_colissimo)
- [Commerce So Colissimo Flexibility](https://www.drupal.org/project/commerce_socolissimo)
- [commerce colissimo shipping](https://www.drupal.org/project/commerce_colissimo_shipping)

This project adds support for Drupal Commerce 2.x, relay pickup point support,
and label printing.
