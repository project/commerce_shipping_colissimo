<?php

namespace Drupal\Tests\commerce_shipping_colissimo\Functional;

use Drupal\commerce_shipping_colissimo\LabelFormat;
use Drupal\commerce_shipping_colissimo\LabelSenderParcelIdSource;
use Drupal\commerce_shipping_colissimo\LabelSize;
use Drupal\commerce_shipping_colissimo\Settings;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Settings form test.
 *
 * @group commerce_shipping
 */
class SettingsFormTest extends BrowserTestBase {
  /**
   * Settings.
   */
  protected Settings $settings;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['commerce_shipping_colissimo'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->settings = $this->container->get(Settings::class);
  }

  /**
   * Access denied test.
   */
  public function testAccessDenied() {
    $this->drupalGet(Url::fromRoute('commerce_shipping_colissimo.settings'));
    $this->assertSession()->responseContains('Access denied');
  }

  /**
   * Save test.
   */
  public function testSave() {
    $anonRole = Role::load(Role::ANONYMOUS_ID);
    $this->grantPermissions($anonRole, ['administer site configuration']);

    $this->drupalGet(Url::fromRoute('commerce_shipping_colissimo.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([
      'user' => 'a',
      'password' => 'b',
      'debug_mode' => 'true',
      'average_preparation_delay_in_days' => '10',
      'label_size' => LabelSize::SIZE_10X15,
      'label_format' => LabelFormat::ZPL,
      'label_sender_parcel_id_source' => LabelSenderParcelIdSource::ORDER_ID,
      'phone_from_billing_profile' => TRUE,
    ], 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);

    $setting = $this->settings->get();

    $this->assertEquals('a', $setting->getUser());
    $this->assertEquals('b', $setting->getPassword());
    $this->assertEquals(TRUE, $setting->isDebugMode());
    $this->assertEquals(10, $setting->getAveragePreparationDelayInDays());
    $this->assertEquals(LabelSize::SIZE_10X15, $setting->getLabelSize());
    $this->assertEquals(LabelFormat::ZPL, $setting->getLabelFormat());
    $this->assertEquals(LabelSenderParcelIdSource::ORDER_ID, $setting->getLabelSenderParcelIdSource());
    $this->assertTrue($setting->isPhoneFromBillingProfile());
  }

}
