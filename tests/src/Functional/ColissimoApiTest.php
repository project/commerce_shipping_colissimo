<?php

namespace Drupal\Tests\commerce_shipping_colissimo\Functional;

use Drupal\commerce_shipping_colissimo\Api\LabelApi;
use Drupal\commerce_shipping_colissimo\Api\WidgetApi;
use Drupal\Tests\BrowserTestBase;

/**
 * Colissimo Api test.
 *
 * @group commerce_shipping
 */
class ColissimoApiTest extends BrowserTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['commerce_shipping_colissimo'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Api initialisation test.
   */
  public function testInit() {
    $this->assertNotNull($this->container->get(WidgetApi::class));
    $this->assertNotNull($this->container->get(LabelApi::class));
  }

}
