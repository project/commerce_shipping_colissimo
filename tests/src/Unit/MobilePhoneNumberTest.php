<?php

namespace Drupal\Tests\commerce_shipping_colissimo\Unit;

use Drupal\commerce_shipping_colissimo\MobilePhoneNumber;
use Drupal\Tests\UnitTestCase;

/**
 * MobilePhoneNumber test.
 *
 * @group commerce_shipping
 */
class MobilePhoneNumberTest extends UnitTestCase {

  /**
   * French phone number test.
   */
  public function testFrenchNumber(): void {
    $number = new MobilePhoneNumber('0606543764 ');
    self::assertTrue($number->isValid());
    self::assertEquals('0606543764', $number->format());
  }

  /**
   * International phone number test.
   */
  public function testInternationalNumber(): void {
    $number = new MobilePhoneNumber('+33606543764 ');
    self::assertTrue($number->isValid());
    self::assertEquals('+33606543764', $number->format());
  }

  /**
   * Too short phone number test.
   */
  public function testTooShortNumber(): void {
    $number = new MobilePhoneNumber('543764');
    self::assertFalse($number->isValid());
  }

  /**
   * Misplaced plus sign test.
   */
  public function testMisplacedPlusSign(): void {
    $number = new MobilePhoneNumber('+54+323423423424764');
    self::assertFalse($number->isValid());
  }

  /**
   * Number with dots test.
   */
  public function testNumberWithDots(): void {
    $number = new MobilePhoneNumber('06.06.54.37.64 ');
    self::assertTrue($number->isValid());
    self::assertEquals('0606543764', $number->format());
  }

}
