<?php

namespace Drupal\Tests\commerce_shipping_colissimo\FunctionalJavascript;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_shipping\ShippingOrderManagerInterface;
use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverWebAssert;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Simulate checkout with colissimo relay selection.
 *
 * Stop before relay selection since it depends on an external lib & service.
 *
 * @group commerce_shipping
 */
class ColissimoRelayCheckoutTest extends CommerceWebDriverTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_shipping_colissimo',
    'commerce_product',
    'commerce_checkout',
  ];

  /**
   * Product.
   */
  protected ProductInterface $product;

  /**
   * Order.
   */
  protected OrderInterface $order;

  /**
   * Shipping order manager.
   */
  protected ShippingOrderManagerInterface $shippingOrderManager;

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions(): array {
    return array_merge([
      'administer commerce_order',
      'administer commerce_shipment',
      'access commerce_order overview',
    ], parent::getAdministratorPermissions());
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $variation_type = ProductVariationType::load('default');
    $variation_type->setTraits(['purchasable_entity_shippable']);
    $variation_type->save();

    $order_type = OrderType::load('default');
    $order_type->setThirdPartySetting('commerce_checkout', 'checkout_flow', 'shipping');
    $order_type->setThirdPartySetting('commerce_shipping', 'shipment_type', 'default');
    $order_type->save();

    // Create the order field.
    $field_definition = commerce_shipping_build_shipment_field_definition($order_type->id());
    $this->container->get('commerce.configurable_field_manager')->createField($field_definition);

    // Install the variation trait.
    $trait_manager = $this->container->get('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('purchasable_entity_shippable');
    $trait_manager->installTrait($trait, 'commerce_product_variation', 'default');

    // Create a non-shippable product/variation type set.
    $variation_type = ProductVariationType::create([
      'id' => 'digital',
      'label' => 'Digital',
      'orderItemType' => 'default',
      'generateTitle' => TRUE,
    ]);
    $variation_type->save();

    $product_type = ProductType::create([
      'id' => 'Digital',
      'label' => 'Digital',
      'variationType' => $variation_type->id(),
    ]);
    $product_type->save();

    // Create two products. One shippable, one non-shippable.
    $variation = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '7.99',
        'currency_code' => 'USD',
      ],
    ]);
    $entity = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'Conference hat',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);
    assert($entity instanceof ProductInterface);
    $this->product = $entity;

    $order_item = $this->createEntity('commerce_order_item', [
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'quantity' => 1,
      'unit_price' => new Price('7.99', 'USD'),
      'purchased_entity' => $this->product->getDefaultVariation(),
    ]);
    $order_item->save();

    $entity = $this->createEntity('commerce_order', [
      'type' => 'default',
      'order_number' => '2020/01',
      'store_id' => $this->store,
      'uid' => $this->adminUser->id(),
      'order_items' => [$order_item],
      'state' => 'draft',
    ]);
    assert($entity instanceof OrderInterface);
    $this->order = $entity;

    $this->createEntity('commerce_shipping_method', [
      'name' => 'Standard shipping',
      'stores' => [$this->store->id()],
      'weight' => -10,
      'plugin' => [
        'target_plugin_id' => 'flat_rate',
        'target_plugin_configuration' => [
          'rate_label' => 'Standard shipping',
          'rate_amount' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ]);

    $this->createEntity('commerce_shipping_method', [
      'name' => 'Colissimo relay shipping',
      'stores' => [$this->store->id()],
      'plugin' => [
        'target_plugin_id' => 'commerce_shipping_colissimo',
        'target_plugin_configuration' => [
          'colissimo_type' => 'relay',
          'rate_label' => 'Colissimo relay shipping',
          'rate_amount' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ]);

    $this->shippingOrderManager = $this->container->get('commerce_shipping.order_manager');
  }

  /**
   * Tests pickup shipping.
   */
  public function testCheckout(): void {
    $this->createEntity('profile', [
      'type' => 'customer',
      'uid' => $this->adminUser->id(),
      'address' => [
        'country_code' => 'FR',
        'locality' => 'Paris',
        'postal_code' => '75002',
        'address_line1' => '38 Rue du Sentier',
        'given_name' => 'Leon',
        'family_name' => 'Blum',
      ],
      'is_default' => TRUE,
    ]);

    $this->drupalGet(Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $this->order->id(),
    ]));
    $this->assertSession()->waitForText('Shipping information');
    $billing_prefix = 'billing_information[profile]';
    $shipping_prefix = 'shipping_information';
    $this->assertSession()->checkboxChecked($billing_prefix . '[copy_fields][enable]');
    $this->assertSession()->fieldExists($shipping_prefix . '[shipping_profile][select_address]');
    $this->getSession()->getPage()->selectFieldOption($shipping_prefix . '[shipments][0][shipping_method][0]', '2--default');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldNotExists($shipping_prefix . '[shipping_profile][address][0][address][address_line1]');
    $this->assertSession()->elementTextEquals('css', '#edit-shipping-information-shipping-profile',
      'Colissimo relay is temporarily unavailable. Please try again later.');
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\FunctionalJavascriptTests\WebDriverWebAssert
   *   WebDriverWebAssert
   */
  public function assertSession($name = NULL) {
    $session = parent::assertSession($name);
    assert($session instanceof WebDriverWebAssert);
    return $session;
  }

}
