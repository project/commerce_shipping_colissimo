/**
 * @typedef {Object} ColissimoWidgetConfig
 * @property {?string} callBackFrame
 * @property {?string} token
 * @property {?string} URLColissimo
 * @property {?string} ceAddress
 * @property {?string} ceCountry
 * @property {?string} ceCountryList
 * @property {?string} ceLang
 * @property {?string} ceTown
 * @property {?string} ceZipCode
 * @property {boolean} geolocated
 *
 * @typedef {Object} ColissimoResponse
 * @property {?string} identifiant
 * @property {?string} nom
 * @property {?string} adresse1
 * @property {?string} adresse2
 * @property {?string} adresse3
 * @property {?string} codePostal
 * @property {?string} localite
 * @property {?string} codePays
 * @property {?string} libellePays
 *
 * @typedef {Object} ColissimoRelayProfileSettings
 * @property {string} token
 * @property {string} baseUrl
 */
(function ($) {

  Drupal.behaviors.commerceShippingColissimoRelayWidget = {
    /**
     *
     * @param context {HTMLElement}
     * @param settings {{commerce_shipping_colissimo: {relayProfile:
     *     ColissimoRelayProfileSettings}}}
     */
    attach: function (context, settings) {

      once('colissimoChooseRelayButton', '.colissimo-choose-relay-btn', context).forEach(function (button) {
        const relaySettings = settings.commerce_shipping_colissimo.relayProfile;

        /** @type {ColissimoWidgetConfig} */
        const defaultWidgetConfig = {
          callBackFrame: 'colissimoRelayWidgetCallback',
          token: relaySettings.token,
          URLColissimo: relaySettings.baseUrl,
          ceAddress: '',
          ceCountry: 'FR',
          ceCountryList: '',
          ceLang: settings.path.currentLanguage,
          ceTown: 'Paris 15',
          ceZipCode: '78015',
          geolocated: false
        };

        function updateRelayTitle() {
          if (!$(context).find('.relay_id').val()) {
            $(context).find('.relay-title').text(Drupal.t('No relay selected.'));
            return;
          }
          let title = '';
          ['given_name', 'address_line1', 'postal_code', 'locality']
              .forEach(field => title += $(context).find('.' + field).val() + '\n')
          $(context).find('.relay-title').text(title);
        }

        /**
         * Optional geolocation via ipinfo.io.
         * @param config {ColissimoWidgetConfig}
         * @return {Promise<ColissimoWidgetConfig>}
         */
        function geolocate(config) {
          if (config.geolocated) {
            return $.when(config);
          }
          config.geolocated = true;
          return $.getJSON("https://ipinfo.io")
              .then(response => {
                if (!response || !response.city || !response.postal || !response.country) {
                  return config;
                }
                config.ceTown = response.city;
                config.ceZipCode = response.postal.replace(/\D/g, '');
                config.ceCountry = response.country;
                return config;
              }).catch(e => {
                console.error('Geolocation query failed', e);
                return config;
              });
        }

        updateRelayTitle();

        $(button).click(function (e) {
          e.preventDefault();
          const container = $('<div>');
          const dialog = Drupal.dialog(container, {
            width: 1100,
            beforeClose: () => container.frameColissimoClose()
          });
          dialog.showModal();

          /**
           * @param response {ColissimoResponse}
           */
          window.colissimoRelayWidgetCallback = function (response) {
            console.log(response);
            dialog.close();

            const relayTitle = response.nom + '\n' + response.adresse1 + '\n' + response.codePostal + ' ' + response.localite + '\n' + response.codePays;
            $(context).find('.relay-title').text(relayTitle);

            const fieldMapping = {
              identifiant: 'relay_id',
              typeDePoint: 'colissimo_product_code',
              nom: 'given_name',
              adresse1: 'address_line1',
              codePostal: 'postal_code',
              localite: 'locality',
              codePays: 'country_code'
            };
            for (const property in fieldMapping) {
              $(context).find('.' + fieldMapping[property]).val(response[property]);
            }
            updateRelayTitle();
          }

          geolocate(defaultWidgetConfig).then(config => container.frameColissimoOpen(config));
        });

      });

    }
  };
})(jQuery);
