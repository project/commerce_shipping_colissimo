<?php

namespace Drupal\commerce_shipping_colissimo;

/**
 * Setting value object.
 */
class Setting {

  /**
   * Base url.
   *
   * @var string
   */
  private string $baseUrl;
  /**
   * User.
   *
   * @var string|null
   */
  private ?string $user = NULL;
  /**
   * Password.
   *
   * @var string|null
   */
  private ?string $password = NULL;
  /**
   * Debug mode.
   *
   * @var bool
   */
  private bool $debugMode;
  /**
   * Average preparation delay in days.
   *
   * @var int
   */
  private int $averagePreparationDelayInDays;
  /**
   * Default parcel weight in kg.
   *
   * @var float
   */
  private float $defaultParcelWeigthInKg;
  /**
   * Label format.
   *
   * @var string
   */
  private string $labelFormat;
  /**
   * Label size.
   *
   * @var string
   */
  private string $labelSize;
  /**
   * Customer profile phone field.
   *
   * @var string|null
   */
  private ?string $customerProfilePhoneField;
  /**
   * Label sender parcel id source.
   *
   * @var string
   */
  private string $labelSenderParcelIdSource;
  /**
   * Phone from billing profile.
   *
   * @var bool
   */
  private bool $phoneFromBillingProfile;

  /**
   * Is configured.
   *
   * @return bool
   *   True if configured, false otherwise.
   */
  public function isConfigured(): bool {
    return $this->user && $this->password;
  }

  /**
   * Get base url.
   *
   * @return string
   *   Base url.
   */
  public function getBaseUrl(): string {
    return $this->baseUrl;
  }

  /**
   * Set base url.
   *
   * @param string $baseUrl
   *   Base url.
   */
  public function setBaseUrl(string $baseUrl): void {
    $this->baseUrl = $baseUrl;
  }

  /**
   * Get user.
   *
   * @return string|null
   *   User.
   */
  public function getUser(): ?string {
    return $this->user;
  }

  /**
   * Set user.
   *
   * @param string|null $user
   *   User.
   */
  public function setUser(?string $user): void {
    $this->user = $user;
  }

  /**
   * Get password.
   *
   * @return string|null
   *   Password.
   */
  public function getPassword(): ?string {
    return $this->password;
  }

  /**
   * Set password.
   *
   * @param string|null $password
   *   Password.
   */
  public function setPassword(?string $password): void {
    $this->password = $password;
  }

  /**
   * Is debug mode.
   *
   * @return bool
   *   True if debug mode, false otherwise.
   */
  public function isDebugMode(): bool {
    return $this->debugMode;
  }

  /**
   * Set debug mode.
   *
   * @param bool $debugMode
   *   True if debug mode, false otherwise.
   */
  public function setDebugMode(bool $debugMode): void {
    $this->debugMode = $debugMode;
  }

  /**
   * Get average preparation delay in days.
   *
   * @return int
   *   Average preparation delay in days.
   */
  public function getAveragePreparationDelayInDays(): int {
    return $this->averagePreparationDelayInDays;
  }

  /**
   * Set average preparation delay in days.
   *
   * @param int $averagePreparationDelayInDays
   *   Average preparation delay in days.
   */
  public function setAveragePreparationDelayInDays(int $averagePreparationDelayInDays): void {
    if ($averagePreparationDelayInDays < 0) {
      throw new \UnexpectedValueException('Preparation delay cannot be negative');
    }
    $this->averagePreparationDelayInDays = $averagePreparationDelayInDays;
  }

  /**
   * Get default parcel weigth in kg.
   *
   * @return float
   *   Default parcel weigth in kg.
   */
  public function getDefaultParcelWeigthInKg(): float {
    return $this->defaultParcelWeigthInKg;
  }

  /**
   * Set default parcel weigth in kg.
   *
   * @param float $defaultParcelWeigthInKg
   *   Default parcel weigth in kg.
   */
  public function setDefaultParcelWeigthInKg(float $defaultParcelWeigthInKg): void {
    if ($defaultParcelWeigthInKg < 0) {
      throw new \UnexpectedValueException('Parcel weight cannot be negative');
    }
    $this->defaultParcelWeigthInKg = $defaultParcelWeigthInKg;
  }

  /**
   * Get label format.
   *
   * @return string
   *   Label format.
   */
  public function getLabelFormat(): string {
    return $this->labelFormat;
  }

  /**
   * Set label format.
   *
   * @param string $labelFormat
   *   Label format.
   */
  public function setLabelFormat(string $labelFormat): void {
    LabelFormat::assertExists($labelFormat);
    $this->labelFormat = $labelFormat;
  }

  /**
   * Get label size.
   *
   * @return string
   *   Label size.
   */
  public function getLabelSize(): string {
    return $this->labelSize;
  }

  /**
   * Set label size.
   *
   * @param string $labelSize
   *   Label size.
   */
  public function setLabelSize(string $labelSize): void {
    LabelSize::assertExists($labelSize);
    $this->labelSize = $labelSize;
  }

  /**
   * Get customer profile phone field.
   *
   * @return string|null
   *   Customer profile phone field.
   */
  public function getCustomerProfilePhoneField(): ?string {
    return $this->customerProfilePhoneField;
  }

  /**
   * Set customer profile phone field.
   *
   * @param string|null $customerProfilePhoneField
   *   Customer profile phone field.
   */
  public function setCustomerProfilePhoneField(?string $customerProfilePhoneField): void {
    $this->customerProfilePhoneField = $customerProfilePhoneField;
  }

  /**
   * Get label sender parcel id source.
   *
   * @return string
   *   Label sender parcel id source.
   */
  public function getLabelSenderParcelIdSource(): string {
    return $this->labelSenderParcelIdSource;
  }

  /**
   * Set label sender parcel id source.
   *
   * @param string $labelSenderParcelIdSource
   *   Label sender parcel id source.
   */
  public function setLabelSenderParcelIdSource(string $labelSenderParcelIdSource): void {
    LabelSenderParcelIdSource::assertExists($labelSenderParcelIdSource);
    $this->labelSenderParcelIdSource = $labelSenderParcelIdSource;
  }

  /**
   * Is phone from billing profile.
   *
   * @return bool
   *   True if phone from billing profile, false otherwise.
   */
  public function isPhoneFromBillingProfile(): bool {
    return $this->phoneFromBillingProfile;
  }

  /**
   * Set phone from billing profile.
   *
   * @param bool $phoneFromBillingProfile
   *   True if phone from billing profile, false otherwise.
   */
  public function setPhoneFromBillingProfile(bool $phoneFromBillingProfile): void {
    $this->phoneFromBillingProfile = $phoneFromBillingProfile;
  }

}
