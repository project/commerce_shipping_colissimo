<?php

namespace Drupal\commerce_shipping_colissimo;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethod;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\commerce_shipping\Form\ShipmentForm;
use Drupal\commerce_shipping\PackerManagerInterface;
use Drupal\commerce_shipping\ShippingMethodStorageInterface;
use Drupal\commerce_shipping_colissimo\Plugin\Commerce\ShippingMethod\Colissimo;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Shared logic for relay form.
 */
class RelayFormHelper {
  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * PackerManager.
   *
   * @var \Drupal\commerce_shipping\PackerManagerInterface
   */
  private PackerManagerInterface $packerManager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, PackerManagerInterface $packerManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->packerManager = $packerManager;
  }

  /**
   * Is colissimo relay.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return bool
   *   True if colissimo relay, false otherwise.
   */
  public function isColissimoRelay(FormStateInterface $formState): bool {
    $shippingMethod = $this->extractSelectedShippingMethod($formState);
    if (!($shippingMethod instanceof ShippingMethod)) {
      return FALSE;
    }
    $plugin = $shippingMethod->getPlugin();
    if (!($plugin instanceof Colissimo)) {
      return FALSE;
    }
    return $plugin->isRelay();
  }

  /**
   * Extract selected shipping method.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return \Drupal\commerce_shipping\Entity\ShippingMethodInterface|null
   *   The selected shipping method.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function extractSelectedShippingMethod(FormStateInterface $formState): ?ShippingMethodInterface {
    $shippingMethodKey = @$formState->getUserInput()['shipping_information']['shipments'][0]['shipping_method'][0]
                    ?: @$formState->getUserInput()['shipping_method'][0];
    if ($shippingMethodKey) {
      $shippingMethodId = (int) filter_var($shippingMethodKey, FILTER_SANITIZE_NUMBER_INT);
      $shippingMethod = $this->entityTypeManager->getStorage('commerce_shipping_method')->load($shippingMethodId);
      assert($shippingMethod instanceof ShippingMethodInterface);
      return $shippingMethod;
    }
    $callback = @$formState->getBuildInfo()['callback_object'];
    if ($callback instanceof ShipmentForm) {
      $shipment = $callback->getEntity();
      assert($shipment instanceof ShipmentInterface);
      return $shipment->getShippingMethod();
    }

    $order = $this->extractOrder($formState);
    $shipmentsField = $order->get('shipments');
    assert($shipmentsField instanceof EntityReferenceFieldItemListInterface);
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
    $shipments = $shipmentsField->referencedEntities();
    if (empty($shipments)) {
      return $this->retrieveExpectedShippingMethod($order);
    }

    $shipment = reset($shipments);
    return $shipment->getShippingMethod();
  }

  /**
   * Retrieve expected shipping method.
   *
   * When an order does not yet have shipments, ask the package manager for
   * the expected shipments, then take the first available shipping method.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   *
   * @return \Drupal\commerce_shipping\Entity\ShippingMethodInterface|null
   *   The expected shipping method.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function retrieveExpectedShippingMethod(OrderInterface $order): ?ShippingMethodInterface {
    $shippingProfile = $this->entityTypeManager->getStorage('profile')->create([
      'type' => 'customer',
      'uid' => 0,
    ]);
    assert($shippingProfile instanceof ProfileInterface);
    $shipments = [];
    [$shipments] = $this->packerManager->packToShipments($order, $shippingProfile, $shipments);
    if (empty($shipments)) {
      return NULL;
    }
    $shippingMethodStorage = $this->entityTypeManager->getStorage('commerce_shipping_method');
    assert($shippingMethodStorage instanceof ShippingMethodStorageInterface);
    $shippingMethods = $shippingMethodStorage->loadMultipleForShipment($shipments[0]);
    return $shippingMethods ? reset($shippingMethods) : NULL;
  }

  /**
   * Extract order.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order.
   */
  private function extractOrder(FormStateInterface $formState): ?OrderInterface {
    $formObject = $formState->getFormObject();
    if (!($formObject instanceof BaseFormIdInterface)) {
      return NULL;
    }

    if ($formObject instanceof EntityFormInterface) {
      $entity = $formObject->getEntity();
      if ($entity instanceof OrderInterface) {
        return $entity;
      }
    }
    if ($formObject instanceof CheckoutFlowInterface) {
      return $formObject->getOrder();
    }

    return NULL;
  }

}
