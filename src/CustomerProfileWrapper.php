<?php

namespace Drupal\commerce_shipping_colissimo;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Customer profile wrapper.
 */
class CustomerProfileWrapper {
  /**
   * Profile.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  private ProfileInterface $profile;

  /**
   * Constructor.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  public function __construct(ProfileInterface $profile) {
    $this->profile = $profile;
  }

  /**
   * Save.
   */
  public function save(): void {
    $this->profile->save();
  }

  /**
   * Get address.
   *
   * @return array|null
   *   Address field value.
   */
  public function getAddress(): ?array {
    return $this->getAddressField()->getValue();
  }

  /**
   * Get address field.
   *
   * @return \Drupal\address\Plugin\Field\FieldType\AddressItem|null
   *   Address field.
   */
  public function getAddressField(): ?AddressItem {
    $field = $this->profile->get('address')->first();
    if (!$field) {
      return NULL;
    }
    assert($field instanceof AddressItem);
    return $field;
  }

  /**
   * Set address.
   *
   * @param array|null $values
   *   Address value.
   */
  public function setAddress(?array $values): void {
    $this->getAddressField()->setValue($values);
  }

  /**
   * Get relay id.
   *
   * @return int|null
   *   Relay id.
   */
  public function getRelayId(): ?string {
    return $this->profile->getData('relay_id');
  }

  /**
   * Set relay id.
   *
   * @param int|null $id
   *   Relay id.
   */
  public function setRelayId(?string $id): void {
    $this->profile->setData('relay_id', $id);
  }

  /**
   * Get relay id.
   *
   * @return int|null
   *   Relay id.
   */
  public function getProductCode(): ?string {
    return $this->profile->getData('colissimo_product_code');
  }

  /**
   * Set relay id.
   *
   * @param int|null $id
   *   Relay id.
   */
  public function setProductCode(?string $id): void {
    $this->profile->setData('colissimo_product_code', $id);
  }

  /**
   * Clear address book.
   */
  public function clearAddressBook(): void {
    $this->profile->unsetData('address_book_profile_id');
    $this->profile->unsetData('copy_to_address_book');
  }

  /**
   * Get mobile phone.
   *
   * @param Setting $setting
   *   Settings.
   *
   * @return string|null
   *   Mobile phone.
   */
  public function getMobilePhone(Setting $setting): ?string {
    if (!$setting->getCustomerProfilePhoneField()) {
      return $this->profile->getData('colissimo_mobile_phone');
    }
    return $this->profile->get($setting->getCustomerProfilePhoneField())->getString();
  }

  /**
   * Set mobile phone.
   *
   * @param string|null $phone
   *   Mobile phone.
   * @param Setting $setting
   *   Settings.
   */
  public function setMobilePhone(?string $phone, Setting $setting): void {
    $mobilePhone = new MobilePhoneNumber($phone);
    if (!$setting->getCustomerProfilePhoneField()) {
      $this->profile->setData('colissimo_mobile_phone', $mobilePhone->format());
      return;
    }
    $this->profile->get($setting->getCustomerProfilePhoneField())->setValue($mobilePhone->format());
  }

}
