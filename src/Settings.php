<?php

namespace Drupal\commerce_shipping_colissimo;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Setting service.
 */
class Settings {
  public const SETTINGS_KEY = 'commerce_shipping_colissimo.settings';

  private const CONF_USER = 'user';

  private const CONF_PASSWORD = 'password';

  private const CONF_DEBUG_MODE = 'debug_mode';

  private const CONF_BASE_URL = 'base_url';

  private const CONF_AVERAGE_PREPARATION_DELAY_IN_DAYS = 'average_preparation_delay_in_days';

  private const CONF_DEFAULT_PARCEL_WEIGHT_IN_KG = 'default_parcel_weight_in_kg';

  private const CONF_LABEL_SIZE = 'label_size';

  private const CONF_LABEL_FORMAT = 'label_format';

  private const CONF_CUSTOMER_PROFILE_PHONE_FIELD = 'customer_profile_phone_field';

  private const CONF_LABEL_SENDER_PARCEL_ID_SOURCE = 'label_sender_parcel_id_source';

  private const CONF_PHONE_FROM_BILLING_PROFILE = 'phone_from_billing_profile';

  /**
   * ConfigFactoryInterface.
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Get settings.
   *
   * @return \Drupal\commerce_shipping_colissimo\Setting
   *   Settings.
   */
  public function get(): Setting {
    $settings = new Setting();
    $config = $this->configFactory->get(self::SETTINGS_KEY);

    $settings->setUser($config->get(self::CONF_USER));
    $settings->setPassword($config->get(self::CONF_PASSWORD));
    $settings->setDebugMode(boolval($config->get(self::CONF_DEBUG_MODE)));
    $settings->setBaseUrl($config->get(self::CONF_BASE_URL) ?: 'https://ws.colissimo.fr');
    $settings->setAveragePreparationDelayInDays($config->get(self::CONF_AVERAGE_PREPARATION_DELAY_IN_DAYS) ?: 3);
    $settings->setDefaultParcelWeigthInKg($config->get(self::CONF_DEFAULT_PARCEL_WEIGHT_IN_KG) ?: 1);
    $settings->setLabelFormat($config->get(self::CONF_LABEL_FORMAT) ?: LabelFormat::PDF);
    $settings->setLabelSize($config->get(self::CONF_LABEL_SIZE) ?: LabelSize::A4);
    $settings->setCustomerProfilePhoneField($config->get(self::CONF_CUSTOMER_PROFILE_PHONE_FIELD));
    $settings->setLabelSenderParcelIdSource($config->get(self::CONF_LABEL_SENDER_PARCEL_ID_SOURCE) ?: LabelSenderParcelIdSource::NONE);
    $settings->setPhoneFromBillingProfile($config->get(self::CONF_PHONE_FROM_BILLING_PROFILE) ?: FALSE);

    return $settings;
  }

  /**
   * Save settings.
   *
   * @param \Drupal\commerce_shipping_colissimo\Setting $settings
   *   Settings.
   */
  public function save(Setting $settings): void {
    $this->configFactory->getEditable(self::SETTINGS_KEY)
      ->set(self::CONF_USER, $settings->getUser())
      ->set(self::CONF_PASSWORD, $settings->getPassword())
      ->set(self::CONF_DEBUG_MODE, $settings->isDebugMode())
      ->set(self::CONF_AVERAGE_PREPARATION_DELAY_IN_DAYS, $settings->getAveragePreparationDelayInDays())
      ->set(self::CONF_DEFAULT_PARCEL_WEIGHT_IN_KG, $settings->getDefaultParcelWeigthInKg())
      ->set(self::CONF_LABEL_SIZE, $settings->getLabelSize())
      ->set(self::CONF_LABEL_FORMAT, $settings->getLabelFormat())
      ->set(self::CONF_CUSTOMER_PROFILE_PHONE_FIELD, $settings->getCustomerProfilePhoneField())
      ->set(self::CONF_LABEL_SENDER_PARCEL_ID_SOURCE, $settings->getLabelSenderParcelIdSource())
      ->set(self::CONF_PHONE_FROM_BILLING_PROFILE, $settings->isPhoneFromBillingProfile())
      ->save();
  }

}
