<?php

namespace Drupal\commerce_shipping_colissimo;

use CommerceGuys\Addressing\AbstractEnum;

/**
 * Label format.
 */
final class LabelFormat extends AbstractEnum {
  const PDF = 'PDF';
  const ZPL = 'ZPL';
  const DPL = 'DPL';

}
