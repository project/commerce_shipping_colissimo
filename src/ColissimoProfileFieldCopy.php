<?php

namespace Drupal\commerce_shipping_colissimo;

use Drupal\commerce_shipping\ProfileFieldCopy;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Override ProfileFieldCopy.
 *
 * If a colissimo relay method is selected, disable billing same as shipping.
 */
final class ColissimoProfileFieldCopy extends ProfileFieldCopy {

  /**
   * RelayFormHelper.
   *
   * @var RelayFormHelper
   */
  protected RelayFormHelper $relayFormHelper;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user.
   * @param RelayFormHelper $relayFormHelper
   *   Relay form helper.
   */
  public function __construct(AccountInterface $currentUser, RelayFormHelper $relayFormHelper) {
    parent::__construct($currentUser);
    $this->relayFormHelper = $relayFormHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsForm(array &$inlineForm, FormStateInterface $formState) {
    if ($this->relayFormHelper->isColissimoRelay($formState)) {
      return FALSE;
    }
    return parent::supportsForm($inlineForm, $formState);
  }

}
