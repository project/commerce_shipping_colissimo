<?php

namespace Drupal\commerce_shipping_colissimo;

use CommerceGuys\Addressing\AbstractEnum;

/**
 * Colissimo type enum.
 */
final class ColissimoType extends AbstractEnum {
  const RELAY = 'relay';
  const SIGNATURE = 'signature';
  const NO_SIGNATURE = 'no_signature';

}
