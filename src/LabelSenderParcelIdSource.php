<?php

namespace Drupal\commerce_shipping_colissimo;

use CommerceGuys\Addressing\AbstractEnum;

/**
 * Label sender parcel id source enum.
 */
final class LabelSenderParcelIdSource extends AbstractEnum {
  const NONE = 'NONE';
  const ORDER_ID = 'ORDER_ID';
  const SHIPMENT_ID = 'SHIPMENT_ID';

}
