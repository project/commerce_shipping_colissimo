<?php

namespace Drupal\commerce_shipping_colissimo;

use Drupal\commerce_shipping_label\RemoteLabelFile;
use Drupal\commerce_shipping_label\RemoteShipment;

/**
 * Label value object.
 */
class Label {

  /**
   * Remote label file.
   *
   * @var \Drupal\commerce_shipping_label\RemoteLabelFile
   */
  private RemoteLabelFile $labelFile;

  /**
   * Remote shipment.
   *
   * @var \Drupal\commerce_shipping_label\RemoteShipment
   */
  private RemoteShipment $shipment;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_shipping_label\RemoteLabelFile $labelFile
   *   Remote label file.
   * @param \Drupal\commerce_shipping_label\RemoteShipment $shipment
   *   Remote shipment.
   */
  public function __construct(RemoteLabelFile $labelFile, RemoteShipment $shipment) {
    $this->labelFile = $labelFile;
    $this->shipment = $shipment;
  }

  /**
   * Get label file.
   *
   * @return \Drupal\commerce_shipping_label\RemoteLabelFile
   *   Label file.
   */
  public function getLabelFile(): RemoteLabelFile {
    return $this->labelFile;
  }

  /**
   * Get shipment.
   *
   * @return \Drupal\commerce_shipping_label\RemoteShipment
   *   Remote shipment.
   */
  public function getShipment(): RemoteShipment {
    return $this->shipment;
  }

}
