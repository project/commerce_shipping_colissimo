<?php

namespace Drupal\commerce_shipping_colissimo\Form;

use Drupal\commerce_shipping_colissimo\LabelFormat;
use Drupal\commerce_shipping_colissimo\LabelSenderParcelIdSource;
use Drupal\commerce_shipping_colissimo\LabelSize;
use Drupal\commerce_shipping_colissimo\Settings;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Settings.
   */
  private Settings $settings;

  /**
   * EntityFieldManager.
   */
  private EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                                EntityFieldManagerInterface $entityFieldManager,
                                Settings $settings) {
    parent::__construct($config_factory);
    $this->settings = $settings;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new SettingsForm(
          $container->get('config.factory'),
          $container->get('entity_field.manager'),
          $container->get(Settings::class)
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_shipping_colissimo_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [Settings::SETTINGS_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $setting = $this->settings->get();
    $form = [
      'api' => [
        '#type' => 'fieldset',
        '#title' => $this->t('API'),
        'user' => [
          '#type' => 'textfield',
          '#title' => $this->t('User'),
          '#default_value' => $setting->getUser(),
          '#required' => TRUE,
        ],
        'password' => [
          '#type' => 'textfield',
          '#title' => $this->t('Password'),
          '#default_value' => $setting->getPassword(),
          '#required' => TRUE,
        ],
        'debug_mode' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Debug mode'),
          '#description' => $this->t('If set, all exchange with colissimo API will be logged.'),
          '#default_value' => $setting->isDebugMode(),
        ],
      ],
      'label' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Labels'),
        'average_preparation_delay_in_days' => [
          '#type' => 'number',
          '#title' => $this->t('Average preparation delay'),
          '#description' => $this->t('In days.'),
          '#default_value' => $setting->getAveragePreparationDelayInDays(),
          '#min' => 0,
          '#required' => TRUE,
        ],
        'default_parcel_weight_in_kg' => [
          '#type' => 'number',
          '#title' => $this->t('Default parcel weight'),
          '#description' => $this->t('In kg. Used if a shipment does not have a non-zero weight.'),
          '#default_value' => $setting->getDefaultParcelWeigthInKg(),
          '#min' => 0,
          '#step' => 0.1,
          '#required' => TRUE,
        ],
        'label_size' => [
          '#type' => 'select',
          '#title' => $this->t('Label size'),
          '#options' => [
            LabelSize::A4 => $this->t('A4'),
            LabelSize::SIZE_10X10 => $this->t('10x10cm'),
            LabelSize::SIZE_10X15 => $this->t('10x15cm'),
          ],
          '#default_value' => $setting->getLabelSize(),
        ],
        'label_format' => [
          '#type' => 'select',
          '#title' => $this->t('Label format'),
          '#options' => [
            LabelFormat::PDF => 'PDF',
            LabelFormat::DPL => 'DPL',
            LabelFormat::ZPL => 'ZPL',
          ],
          '#default_value' => $setting->getLabelFormat(),
        ],
        'customer_profile_phone_field' => [
          '#type' => 'textfield',
          '#title' => $this->t('Phone field on customer profile'),
          '#description' => $this->t('By default, the phone will be stored as a profile data attribute. You may instead specify to use a custom field by name.'),
          '#default_value' => $setting->getCustomerProfilePhoneField(),
        ],
        'phone_from_billing_profile' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Use billing profile for phone field.'),
          '#description' => $this->t('Do not ask for a phone number. Instead read it from the billing profile using the above field name.'),
          '#default_value' => $setting->isPhoneFromBillingProfile(),
        ],
        'label_sender_parcel_id_source' => [
          '#type' => 'select',
          '#title' => $this->t('Sender parcel barcode source'),
          '#description' => $this->t('Adds an addition barcode on the shipping label. This barcode is not used by colissimo and is intended for the sender internal use.'),
          '#options' => [
            LabelSenderParcelIdSource::NONE => $this->t('None'),
            LabelSenderParcelIdSource::ORDER_ID => $this->t('Order Id'),
            LabelSenderParcelIdSource::SHIPMENT_ID => $this->t('Shipment Id'),
          ],
          '#default_value' => $setting->getLabelSenderParcelIdSource(),
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Validate form.
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    if (LabelSize::A4 == $formState->getValue('label_size')
          && LabelFormat::PDF != $formState->getValue('label_format')) {
      $formState->setErrorByName('label_size', $this->t('A4 label size is only available in PDF format.'));
    }
    $phoneField = $formState->getValue('customer_profile_phone_field');
    if ($phoneField) {
      $customerFields = $this->entityFieldManager->getFieldDefinitions('profile', 'customer');
      if (empty($customerFields[$phoneField])) {
        $formState->setErrorByName(
              'customer_profile_phone_field',
              $this->t('The field %field was not found on customer profile entity.', ['%field' => $phoneField])
          );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $setting = $this->settings->get();
    $setting->setUser($formState->getValue('user'));
    $setting->setPassword($formState->getValue('password'));
    $setting->setDebugMode(boolval($formState->getValue('debug_mode')));
    $setting->setAveragePreparationDelayInDays(intval($formState->getValue('average_preparation_delay_in_days')));
    $setting->setDefaultParcelWeigthInKg(floatval($formState->getValue('default_parcel_weight_in_kg')));
    $setting->setLabelFormat($formState->getValue('label_format'));
    $setting->setLabelSize($formState->getValue('label_size'));
    $setting->setCustomerProfilePhoneField($formState->getValue('customer_profile_phone_field'));
    $setting->setLabelSenderParcelIdSource($formState->getValue('label_sender_parcel_id_source'));
    $setting->setPhoneFromBillingProfile($formState->getValue('phone_from_billing_profile'));
    $this->settings->save($setting);
    parent::submitForm($form, $formState);
  }

}
