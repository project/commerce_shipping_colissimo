<?php

namespace Drupal\commerce_shipping_colissimo;

use Drupal\address\AddressInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping_colissimo\Api\ColissimoApiException;
use Drupal\commerce_shipping_colissimo\Api\LabelApi;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationAddress;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationAddressee;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationLetter;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationOutputFormat;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationParcel;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationPayload;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationSender;
use Drupal\commerce_shipping_colissimo\Api\LabelGenerationService;
use Drupal\commerce_shipping_colissimo\Plugin\Commerce\ShippingMethod\Colissimo;
use Drupal\commerce_shipping_label\RemoteLabelFile;
use Drupal\commerce_shipping_label\RemoteShipment;
use Drupal\commerce_shipping_label\ShippingLabelGenerationException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use Drupal\file\FileRepositoryInterface;
use Drupal\physical\WeightUnit;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Label service.
 */
class Labels {
  use StringTranslationTrait;

  /**
   * Label api.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelApi
   */
  private LabelApi $labelApi;

  /**
   * Settings.
   *
   * @var Settings
   */
  private Settings $settings;

  /**
   * ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * LoggerChannelInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * FileRepositoryInterface.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  private FileRepositoryInterface $fileRepository;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_shipping_colissimo\Api\LabelApi $labelApi
   *   Label api.
   * @param \Drupal\commerce_shipping_colissimo\Settings $settings
   *   Settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @param \Drupal\file\FileRepositoryInterface $fileRepository
   *   File repository.
   */
  public function __construct(LabelApi $labelApi,
                              Settings $settings,
                              ConfigFactoryInterface $configFactory,
                              LoggerChannelInterface $logger,
                              FileRepositoryInterface $fileRepository) {
    $this->labelApi = $labelApi;
    $this->settings = $settings;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
    $this->fileRepository = $fileRepository;
  }

  /**
   * Generate label.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return \Drupal\commerce_shipping_colissimo\Label
   *   Generated label.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\commerce_shipping_label\ShippingLabelGenerationException
   */
  public function generate(ShipmentInterface $shipment): Label {
    $payload = new LabelGenerationPayload();
    $payload->letter = $this->buildLetter($shipment);
    $payload->outputFormat = $this->buildOutputFormat();

    try {
      $response = $this->labelApi->generate($payload);
    }
    catch (ColissimoApiException $e) {
      throw new ShippingLabelGenerationException('Colissimo: ' . $e->getMessage());
    }
    catch (GuzzleException $e) {
      $this->logger->error(Error::renderExceptionSafe($e));
      // phpcs:ignore
      throw new ShippingLabelGenerationException($this->t('Failed to generate colissimo label.'));
    }
    if (!$response->labelV2Response
      || !$response->labelV2Response->parcelNumber
      || !$response->files) {
      // phpcs:ignore
      throw new ShippingLabelGenerationException($this->t('Colissimo failed to return a parcel number or label file.'));
    }
    $parcelNumber = $response->labelV2Response->parcelNumber;

    $file = $this->fileRepository->writeData(
      $response->files[0],
      'temporary://colissimo_label_' . $parcelNumber . '.' . $this->settings->get()->getLabelFormat()
    );
    $file->setTemporary();
    $file->save();

    return new Label(
      new RemoteLabelFile($file->getFileUri(), $this->t('Colissimo Label')),
      new RemoteShipment($parcelNumber, $parcelNumber));
  }

  /**
   * Build letter.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return \Drupal\commerce_shipping_colissimo\Api\LabelGenerationLetter
   *   Label generation letter.
   *
   * @throws \Drupal\commerce_shipping_label\ShippingLabelGenerationException
   */
  private function buildLetter(ShipmentInterface $shipment): LabelGenerationLetter {
    $letter = new LabelGenerationLetter();
    $letter->service = $this->buildService($shipment);
    $letter->parcel = $this->buildParcel($shipment);
    $letter->sender = $this->buildSender($shipment);
    $letter->addressee = $this->buildAddressee($shipment);
    return $letter;
  }

  /**
   * Build output format.
   *
   * @return \Drupal\commerce_shipping_colissimo\Api\LabelGenerationOutputFormat
   *   Label generation output format.
   */
  private function buildOutputFormat(): LabelGenerationOutputFormat {
    $format = new LabelGenerationOutputFormat();
    $format->x = 0;
    $format->y = 0;
    $format->outputPrintingType = $this->getOutputPrintintType();
    return $format;
  }

  /**
   * Build service.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return \Drupal\commerce_shipping_colissimo\Api\LabelGenerationService
   *   Label generation service.
   */
  private function buildService(ShipmentInterface $shipment): LabelGenerationService {
    $service = new LabelGenerationService();
    $method = $shipment->getShippingMethod()->getPlugin();
    assert($method instanceof Colissimo);
    $service->productCode = $this->getProductCode($method, $shipment);

    $setting = $this->settings->get();
    $depositDate = new \DateTime();
    $depositDate = $depositDate->modify('+' . $setting->getAveragePreparationDelayInDays() . ' days');
    $service->depositDate = $depositDate;

    $service->commercialName = $this->configFactory->get('system.site')->get('name');

    return $service;
  }

  /**
   * Build parcel.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return \Drupal\commerce_shipping_colissimo\Api\LabelGenerationParcel
   *   Label generation parcel.
   */
  private function buildParcel(ShipmentInterface $shipment): LabelGenerationParcel {
    $parcel = new LabelGenerationParcel();
    $weight = $shipment->getWeight();
    $parcel->weight = $weight ? floatval($weight->convert(WeightUnit::KILOGRAM)->getNumber()) : 0;
    if ($parcel->weight < 0.01) {
      $parcel->weight = $this->settings->get()->getDefaultParcelWeigthInKg();
    }

    if ($this->isRelay($shipment)) {
      $customerProfile = new CustomerProfileWrapper($shipment->getShippingProfile());
      $parcel->pickupLocationId = $customerProfile->getRelayId();
    }

    return $parcel;
  }

  /**
   * Is relay.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return bool
   *   Return true if the shipment is a relay.
   */
  private function isRelay(ShipmentInterface $shipment) : bool {
    $method = $shipment->getShippingMethod()->getPlugin();
    assert($method instanceof Colissimo);
    return $method->isRelay();
  }

  /**
   * Build sender.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return \Drupal\commerce_shipping_colissimo\Api\LabelGenerationSender
   *   Label generation sender.
   */
  private function buildSender(ShipmentInterface $shipment): LabelGenerationSender {
    $sender = new LabelGenerationSender();
    switch ($this->settings->get()->getLabelSenderParcelIdSource()) {
      case LabelSenderParcelIdSource::ORDER_ID:
        $sender->senderParcelRef = $shipment->getOrder()->id();
        break;

      case LabelSenderParcelIdSource::SHIPMENT_ID:
        $sender->senderParcelRef = $shipment->id();
        break;
    }
    $store = $shipment->getOrder()->getStore();
    $sender->address = $this->buildAddress($store->getAddress());
    $sender->address->companyName = $store->getName();
    $sender->address->email = $store->getEmail();
    return $sender;
  }

  /**
   * Build addressees.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return \Drupal\commerce_shipping_colissimo\Api\LabelGenerationAddressee
   *   Label generation addressee.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\commerce_shipping_label\ShippingLabelGenerationException
   */
  private function buildAddressee(ShipmentInterface $shipment): LabelGenerationAddressee {
    $addressee = new LabelGenerationAddressee();
    $customerProfile = new CustomerProfileWrapper($shipment->getShippingProfile());
    if (!$this->isRelay($shipment)) {
      $addressee->address = $this->buildAddress($customerProfile->getAddressField());
    }
    else {
      $billingProfile = $shipment->getOrder()->getBillingProfile();
      if (!$billingProfile || $billingProfile->get('address')->isEmpty()) {
        // phpcs:ignore
        throw new ShippingLabelGenerationException($this->t('A billing profile is mandatory to generate a colissimo label with realy pickup.'));
      }
      $address = $billingProfile->get('address')->first();
      assert($address instanceof AddressInterface);
      $addressee->address = $this->buildAddress($address);
    }
    $addressee->address->email = $shipment->getOrder()->getEmail();
    $setting = $this->settings->get();

    if (!$setting->isPhoneFromBillingProfile()) {
      $phoneNumber = new MobilePhoneNumber($customerProfile->getMobilePhone($setting));
      $addressee->address->mobileNumber = $phoneNumber->format();
      return $addressee;
    }
    $billingProfile = $shipment->getOrder()->getBillingProfile();
    if (!$billingProfile) {
      return $addressee;
    }
    $phoneNumber = new MobilePhoneNumber($billingProfile->get($setting->getCustomerProfilePhoneField())->getString());
    $addressee->address->mobileNumber = $phoneNumber->format();
    return $addressee;
  }

  /**
   * Build addressee.
   *
   * @param \Drupal\address\AddressInterface $address
   *   Address.
   *
   * @return \Drupal\commerce_shipping_colissimo\Api\LabelGenerationAddress
   *   Label generation addressee.
   */
  private function buildAddress(AddressInterface $address): LabelGenerationAddress {
    $labelAddress = new LabelGenerationAddress();
    $labelAddress->companyName = $address->getOrganization();
    $labelAddress->firstName = $address->getGivenName();
    $labelAddress->lastName = $address->getFamilyName();
    $labelAddress->line2 = $address->getAddressLine1();
    $labelAddress->line3 = $address->getAddressLine2();
    $labelAddress->city = $address->getLocality();
    $labelAddress->zipCode = $address->getPostalCode();
    $labelAddress->countryCode = $address->getCountryCode();
    $labelAddress->stateOrProvinceCode = $address->getAdministrativeArea();
    return $labelAddress;
  }

  /**
   * Get product code.
   *
   * @param \Drupal\commerce_shipping_colissimo\Plugin\Commerce\ShippingMethod\Colissimo $method
   *   Colissimo method.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return string
   *   Product code.
   */
  private function getProductCode(Colissimo $method, ShipmentInterface $shipment): string {
    switch ($method->getColissimoType()) {
      case ColissimoType::RELAY:
        $customerProfile = new CustomerProfileWrapper($shipment->getShippingProfile());
        return $customerProfile->getProductCode() ?: 'A2P';

      case ColissimoType::SIGNATURE:
        return 'DOS';

      case ColissimoType::NO_SIGNATURE:
        return 'DOM';
    }
    throw new \UnexpectedValueException('Unknown colissimo type ' . $method->getColissimoType());
  }

  /**
   * Get output printing type.
   *
   * @return string
   *   Output printing type.
   */
  private function getOutputPrintintType(): string {
    $mapping = [
      LabelFormat::PDF => [
        LabelSize::A4 => 'PDF_10x15_300dpi',
        LabelSize::SIZE_10X15 => 'PDF_10x15_300dpi',
        LabelSize::SIZE_10X10 => 'PDF_10x10_300dpi',
      ],
      LabelFormat::ZPL => [
        LabelSize::SIZE_10X15 => 'ZPL_10x15_300dpi',
        LabelSize::SIZE_10X10 => 'ZPL_10x10_300dpi',
      ],
      LabelFormat::DPL => [
        LabelSize::SIZE_10X15 => 'DPL_10x15_300dpi',
        LabelSize::SIZE_10X10 => 'DPL_10x10_300dpi',
      ],
    ];
    $setting = $this->settings->get();
    /** @var ?string $printingType */
    $printingType = @$mapping[$setting->getLabelFormat()][$setting->getLabelSize()];
    if (!$printingType) {
      throw new \UnexpectedValueException('Incoherent label format/size settings.');
    }
    return $printingType;
  }

}
