<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation parcel.
 */
class LabelGenerationParcel {
  /**
   * Weight.
   *
   * @var float
   */
  public $weight;
  /**
   * Pickup location id.
   *
   * @var string
   */
  public $pickupLocationId;

}
