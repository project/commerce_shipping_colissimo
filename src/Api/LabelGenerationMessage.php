<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation message.
 */
class LabelGenerationMessage {
  /**
   * Id.
   *
   * @var string
   */
  public $id;
  /**
   * Message content.
   *
   * @var string
   */
  public $messageContent;
  /**
   * Type.
   *
   * @var string
   */
  public $type;

}
