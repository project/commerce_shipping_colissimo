<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Colissimo Api exception.
 */
class ColissimoApiException extends \Exception {
}
