<?php

namespace Drupal\commerce_shipping_colissimo\Api;

use Drupal\commerce_shipping_colissimo\Settings;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Map widget Api.
 */
class WidgetApi {

  /**
   * MultipartRestClient.
   */
  private MultipartRestClient $multipartRestClient;

  /**
   * CacheBackendInterface.
   */
  private CacheBackendInterface $cacheBackend;

  /**
   * Settings.
   */
  private Settings $settings;

  /**
   * Constructor.
   */
  public function __construct(MultipartRestClient $multipartRestClient, CacheBackendInterface $cacheBackend, Settings $settings) {
    $this->multipartRestClient = $multipartRestClient;
    $this->cacheBackend = $cacheBackend;
    $this->settings = $settings;
  }

  /**
   * Get widget js url.
   */
  public function getWidgetJsUrl(): string {
    return $this->settings->get()->getBaseUrl() . '/widget-colissimo/js/jquery.plugin.colissimo.js';
  }

  /**
   * Set widget js url.
   */
  public function getWidgetAuthenticationToken(): string {
    $cacheEntry = $this->cacheBackend->get('colissimo_widget_api_token');
    if ($cacheEntry) {
      return $cacheEntry->data;
    }
    $response = $this->multipartRestClient->query(
      '/widget-colissimo/rest/authenticate.rest',
      NULL,
      WidgetAuthenticationResponse::class,
      'login')->getData();
    assert($response instanceof WidgetAuthenticationResponse);
    if (empty($response->token)) {
      throw new \UnexpectedValueException('The colissimo widget service returned an empty token! You may have invalid account credentials.');
    }
    $token = $response->token;
    $this->cacheBackend->set('colissimo_widget_api_token', $token, time() + 5 * 60);
    return $token;
  }

}
