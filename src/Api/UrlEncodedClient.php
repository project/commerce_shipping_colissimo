<?php

namespace Drupal\commerce_shipping_colissimo\Api;

use Drupal\commerce_shipping_colissimo\Settings;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Url encoded client.
 */
class UrlEncodedClient {

  /**
   * Http client.
   */
  private ClientInterface $httpClient;

  /**
   * Settings.
   */
  private Settings $settings;

  /**
   * Serializer.
   */
  private Serializer $serializer;

  /**
   * LoggerChannelInterface.
   */
  private LoggerChannelInterface $logger;

  /**
   * Constructor.
   */
  public function __construct(ClientInterface $httpClient, LoggerChannelInterface $logger, Settings $settings) {
    $this->httpClient = $httpClient;
    $this->settings = $settings;
    $this->serializer = new Serializer([
       // Handle objects with public properties, note PhpDocExtractor that
       // allows nested object deserialization via PhpDoc.
      new PropertyNormalizer(NULL, NULL, new PhpDocExtractor()),
    ], [new JsonEncoder()]);
    $this->logger = $logger;
  }

  // phpcs:disable
  /**
   * Perform query.
   * PHP cs disabled as it does not support phpstan's parameterized class
   * syntax.
   *
   * @template T
   *
   * @param class-string<T>|null $returnClass
   *   return class.
   * @param string $path
   *   path.
   * @param object|null $payload
   *   payload.
   *
   * @return T|null
   */
  public function query(string $path, ?object $payload = NULL, ?string $returnClass = NULL) {
    $setting = $this->settings->get();
    $url = $setting->getBaseUrl() . $path;

    $options = [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Accept' => 'application/json',
      ],
    ];

    $setting = $this->settings->get();
    $payload = $this->normalize($payload);
    $payload['login'] = $setting->getUser();
    $payload['password'] = $setting->getPassword();

    $options[RequestOptions::BODY] = http_build_query($payload);

    $httpMethod = 'POST';
    if ($setting->isDebugMode()) {
      $this->logger->debug('Sending %httpMethod request to %url: %options', [
        '%url' => $url,
        '%httpMethod' => $httpMethod,
        '%options' => print_r($options, TRUE),
      ]);
    }

    $httpResponse = $this->httpClient->request($httpMethod, $url, $options);
    $body = $httpResponse->getBody()->getContents();

    if ($setting->isDebugMode()) {
      $this->logger->debug('Received response: %response', [
        '%response' => $body,
      ]);
    }

    if ($returnClass) {
      /** @var T $response */
      $response = $this->serializer->deserialize($body, $returnClass, 'json');
      return $response;
    }
    return NULL;
  }
  // phpcs:enable

  /**
   * Normalize.
   */
  private function normalize(?object $object): array {
    if (!$object) {
      return [];
    }
    return $this->serializer->normalize($object);
  }

}
