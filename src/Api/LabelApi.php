<?php

namespace Drupal\commerce_shipping_colissimo\Api;

use GuzzleHttp\Exception\ClientException;

/**
 * Label generation Api.
 */
class LabelApi {

  /**
   * Multipart rest client.
   */
  private MultipartRestClient $multipartRestClient;

  /**
   * Constructor.
   */
  public function __construct(MultipartRestClient $multipartRestClient) {
    $this->multipartRestClient = $multipartRestClient;
  }

  /**
   * Generate label.
   *
   * @throws \Drupal\commerce_shipping_colissimo\Api\ColissimoApiException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function generate(LabelGenerationPayload $payload): LabelGenerationResponse {
    try {
      $multipartResponse = $this->multipartRestClient->query('/sls-ws/SlsServiceWSRest/2.0/generateLabel', $payload, LabelGenerationResponse::class);
      $labelGenerationResponse = $multipartResponse->getData();
      assert($labelGenerationResponse instanceof LabelGenerationResponse);
      $labelGenerationResponse->files = $multipartResponse->getFiles();
      return $labelGenerationResponse;
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      if (!$response) {
        throw $e;
      }
      $body = $response->getBody()->getContents();
      if (!$body) {
        throw $e;
      }
      $multipartResponse = $this->multipartRestClient->parseMultiPartBody($body, LabelGenerationResponse::class);
      $labelGenerationResponse = $multipartResponse->getData();
      if (empty($labelGenerationResponse)) {
        throw $e;
      }
      assert($labelGenerationResponse instanceof LabelGenerationResponse);
      if (empty($labelGenerationResponse->messages[0]->messageContent)) {
        throw $e;
      }
      throw new ColissimoApiException($labelGenerationResponse->messages[0]->messageContent, 0, $e);
    }
  }

}
