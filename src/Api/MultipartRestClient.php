<?php

namespace Drupal\commerce_shipping_colissimo\Api;

use Drupal\commerce_shipping_colissimo\Settings;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Multipart rest client.
 */
class MultipartRestClient {

  /**
   * HTTP client.
   */
  private ClientInterface $httpClient;

  /**
   * Settings.
   */
  private Settings $settings;

  /**
   * Serializer.
   */
  private Serializer $serializer;

  /**
   * LoggerChannelInterface.
   */
  private LoggerChannelInterface $logger;

  /**
   * Constructor.
   */
  public function __construct(ClientInterface $httpClient, LoggerChannelInterface $logger, Settings $settings) {
    $this->httpClient = $httpClient;
    $this->settings = $settings;
    $this->serializer = new Serializer([
      new ArrayDenormalizer(),
      new DateTimeNormalizer([DateTimeNormalizer::FORMAT_KEY => 'Y-m-d']),
      new PropertyNormalizer(NULL, NULL, new PhpDocExtractor()),
    ], [new JsonEncoder()]);
    $this->logger = $logger;
  }

  /**
   * Perform a query.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function query(string $path, ?object $payload = NULL, ?string $responseClass = NULL, $loginField = 'contractNumber'): MultipartResponse {
    $setting = $this->settings->get();
    $url = $setting->getBaseUrl() . $path;

    $options = [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json',
      ],
    ];

    $setting = $this->settings->get();
    $payload = $this->normalize($payload);
    $payload[$loginField] = $setting->getUser();
    $payload['password'] = $setting->getPassword();

    $options[RequestOptions::BODY] = $this->serializer->serialize($payload, 'json');

    $httpMethod = 'POST';
    if ($setting->isDebugMode()) {
      $this->logger->debug('Sending %httpMethod request to %url: %options', [
        '%url' => $url,
        '%httpMethod' => $httpMethod,
        '%options' => print_r($options, TRUE),
      ]);
    }

    $httpResponse = $this->httpClient->request($httpMethod, $url, $options);
    $body = $httpResponse->getBody()->getContents();

    if ($setting->isDebugMode()) {
      $this->logger->debug('Received response: %response', [
        '%response' => $body,
      ]);
    }

    return $this->parseMultiPartBody($body, $responseClass);
  }

  /**
   * Normalize.
   */
  private function normalize(?object $object): array {
    if (!$object) {
      return [];
    }
    return $this->serializer->normalize($object);
  }

  /**
   * Parse multipart body.
   */
  public function parseMultiPartBody($body, ?string $responseClass): MultipartResponse {
    preg_match('/--(.*)\b/', $body, $boundaries);
    $boundary = @$boundaries[0];

    if (empty($boundary)) {
      return $this->parseMonoPartBody($body, $responseClass);
    }

    $messages = array_filter(
      array_map(
        'trim',
        explode($boundary, $body)
      )
    );

    $parsedData = NULL;
    $files = [];

    foreach ($messages as $message) {
      if ('--' === $message) {
        break;
      }

      $headers = [];
      [$headerLines, $body] = explode("\r\n\r\n", $message, 2);

      foreach (explode("\r\n", $headerLines) as $headerLine) {
        [$key, $value] = preg_split('/:\s+/', $headerLine, 2);
        $headers[strtolower($key)] = $value;
      }

      if (str_contains($headers['content-type'], 'application/json')) {
        if ($responseClass) {
          $parsedData = $this->serializer->deserialize($body, $responseClass, 'json');
        }
      }
      else {
        $files[] = $body;
      }
    }

    return new MultipartResponse($parsedData, $files);
  }

  /**
   * Parse monopart body.
   */
  private function parseMonoPartBody(string $body, $responseClass): MultipartResponse {
    if ($responseClass) {
      $response = $this->serializer->deserialize($body, $responseClass, 'json');
      return new MultipartResponse($response, []);
    }
    return new MultipartResponse(NULL, []);

  }

}
