<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Multipart response.
 */
class MultipartResponse {

  /**
   * Data.
   *
   * @var mixed
   */
  private $data;

  /**
   * Files.
   */
  private array $files;

  /**
   * Constructor.
   */
  public function __construct($data, array $files) {
    $this->data = $data;
    $this->files = $files;
  }

  /**
   * Get data.
   *
   * @return mixed
   *   Deserialized response.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Get files.
   */
  public function getFiles(): array {
    return $this->files;
  }

}
