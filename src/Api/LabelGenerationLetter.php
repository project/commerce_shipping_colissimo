<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation letter.
 */
class LabelGenerationLetter {
  /**
   * Service.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationService
   */
  public $service;
  /**
   * Parcel.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationParcel
   */
  public $parcel;
  /**
   * Sender.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationSender
   */
  public $sender;
  /**
   * Addressee.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationAddressee
   */
  public $addressee;

}
