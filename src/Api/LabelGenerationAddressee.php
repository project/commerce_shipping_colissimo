<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation addressee.
 */
class LabelGenerationAddressee {
  /**
   * Addressee parcel reference.
   *
   * @var string*/
  public $addresseeParcelRef;
  /**
   * Barcode for reference.
   *
   * @var bool
   */
  public $codeBarForReference;
  /**
   * Address.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationAddress
   */
  public $address;

}
