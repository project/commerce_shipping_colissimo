<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation payload.
 */
class LabelGenerationPayload {
  /**
   * Output format.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationOutputFormat
   */
  public $outputFormat;

  /**
   * Label generation letter.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationLetter
   */
  public $letter;

}
