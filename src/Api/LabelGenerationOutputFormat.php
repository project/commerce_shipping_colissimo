<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation output format.
 */
class LabelGenerationOutputFormat {
  /**
   * X.
   *
   * @var int
   */
  public $x;

  /**
   * Y.
   *
   * @var int
   */
  public $y;

  /**
   * Output printing type.
   *
   * @var string
   */
  public $outputPrintingType;

}
