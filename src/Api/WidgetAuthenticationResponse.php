<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Widget authentication response.
 */
class WidgetAuthenticationResponse {
  /**
   * Token.
   *
   * @var string
   */
  public $token;

}
