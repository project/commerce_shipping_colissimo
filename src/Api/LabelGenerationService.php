<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation service.
 */
class LabelGenerationService {
  /**
   * Product code.
   *
   * @var string
   */
  public $productCode;
  /**
   * Deposit date.
   *
   * @var \DateTime
   */
  public $depositDate;
  /**
   * Commercial name.
   *
   * @var string
   */
  public $commercialName;

}
