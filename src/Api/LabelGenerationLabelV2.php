<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation label V2.
 */
class LabelGenerationLabelV2 {
  /**
   * Parcel number.
   *
   * @var string
   */
  public $parcelNumber;

}
