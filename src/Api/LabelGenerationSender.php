<?php

namespace Drupal\commerce_shipping_colissimo\Api;

/**
 * Label generation sender.
 */
class LabelGenerationSender {
  /**
   * Sender parcel reference.
   *
   * @var string
   */
  public $senderParcelRef;
  /**
   * Address.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\LabelGenerationAddress
   */
  public $address;

}
