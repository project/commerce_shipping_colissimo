<?php

namespace Drupal\commerce_shipping_colissimo\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_shipping\OrderShipmentSummaryInterface;
use Drupal\commerce_shipping\PackerManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\CheckoutPane\ShippingInformation;
use Drupal\commerce_shipping_colissimo\CustomerProfileWrapper;
use Drupal\commerce_shipping_colissimo\RelayFormHelper;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overwrite the shipping pane.
 *
 * Adds ajax refresh on shipping method change,
 * and reset profile data if a non-relay method is selected.
 */
class ColissimoShippingInformation extends ShippingInformation {

  /**
   * FormStateInterface.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected FormStateInterface $currentFormState;
  /**
   * RelayFormHelper.
   *
   * @var \Drupal\commerce_shipping_colissimo\RelayFormHelper
   */
  protected RelayFormHelper $formHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition, CheckoutFlowInterface $checkoutFlow = NULL) {
    $instance = parent::create($container, $configuration, $pluginId, $pluginDefinition, $checkoutFlow);
    $instance->formHelper = $container->get(RelayFormHelper::class);
    return $instance;
  }

  /**
   * Sadly this functions code had to be copy pasted with only few replacements.
   *
   * {@inheritdoc}
   */
  public function buildPaneForm(array $paneForm, FormStateInterface $formState, array &$completeForm) {
    $this->currentFormState = $formState;
    $paneForm = parent::buildPaneForm($paneForm, $formState, $completeForm);

    if (empty($paneForm['shipments'])) {
      return $paneForm;
    }

    // Refresh the ShippingInformation pane when a shipment is selected
    // So that we can display the relay select button.
    foreach ($paneForm['shipments'] as $key => &$shipment) {
      if (!is_int($key)) {
        continue;
      }
      $widget = &$shipment['shipping_method']['widget'][0];
      $widget['#ajax'] = [
        'callback' => [static::class , 'ajaxRefreshForm'],
        'element' => $widget['#field_parents'],
      ];
    }

    return $paneForm;
  }

  /**
   * {@inheritdoc}
   */
  protected function getShippingProfile() {
    $profile = parent::getShippingProfile();

    $wrapper = new CustomerProfileWrapper($profile);
    $isRelay = $this->formHelper->isColissimoRelay($this->currentFormState);

    $overwriteProfile = ($isRelay && !$wrapper->getRelayId())
        || (!$isRelay && $wrapper->getRelayId());

    if ($overwriteProfile) {
      $profile = $this->entityTypeManager->getStorage('profile')->create([
        'type' => $profile->bundle(),
        'uid' => 0,
      ]);
      assert($profile instanceof ProfileInterface);
    }

    return $profile;
  }

  /**
   * {@inheritdoc}
   */
  protected function canCalculateRates(ProfileInterface $profile) {
    return TRUE;
  }

}
