<?php

namespace Drupal\commerce_shipping_colissimo\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\FlatRate;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_shipping_colissimo\ColissimoType;
use Drupal\commerce_shipping_colissimo\Labels;
use Drupal\commerce_shipping_colissimo\Settings;
use Drupal\commerce_shipping_label\RemoteLabelFile;
use Drupal\commerce_shipping_label\RemoteShipment;
use Drupal\commerce_shipping_label\SupportsImportingShippingLabelsInterface;
use Drupal\commerce_shipping_label\SupportsRemoteShipmentsInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Colissimo Pickup shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "commerce_shipping_colissimo",
 *   label = @Translation("Colissimo"),
 * )
 */
class Colissimo extends FlatRate implements SupportsRemoteShipmentsInterface, SupportsImportingShippingLabelsInterface, SupportsTrackingInterface {

  /**
   * Settings.
   *
   * @var \Drupal\commerce_shipping_colissimo\Settings
   */
  private Settings $settings;

  /**
   * Labels.
   *
   * @var \Drupal\commerce_shipping_colissimo\Labels
   */
  private Labels $labels;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_shipping_colissimo\Settings $settings
   *   The settings.
   * @param \Drupal\commerce_shipping_colissimo\Labels $labels
   *   The labels.
   */
  final public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
                              PackageTypeManagerInterface $package_type_manager,
                              WorkflowManagerInterface $workflow_manager,
                              Settings $settings,
  Labels $labels) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->services['default'] = new ShippingService('default', $this->configuration['rate_label']);
    $this->settings = $settings;
    $this->labels = $labels;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get(Settings::class),
      $container->get(Labels::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['colissimo_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Colissimo shipping type'),
      '#default_value' => @$this->configuration['colissimo_type'],
      '#options' => [
        ColissimoType::RELAY => $this->t('Colissimo relay'),
        'signature' => $this->t('Colissimo with signature'),
        'no_signature' => $this->t('Colissimo without signature'),
      ],
      '#required' => TRUE,
      '#weight' => 0,
    ];

    return $form;
  }

  /**
   * Validate form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if (!$this->settings->get()->isConfigured()) {
      $form_state->setError($form,
        $this->t('You must <a href="@link">configure</a> your colissimo account first.',
          [
            '@link' => Url::fromRoute('commerce_shipping_colissimo.settings')
              ->toString(),
          ]
        ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['colissimo_type'] = $values['colissimo_type'];
    }
  }

  /**
   * Get colissimo type.
   *
   * @return string
   *   See @see ColissimoType
   */
  public function getColissimoType(): string {
    return $this->configuration['colissimo_type'];
  }

  /**
   * Is relay shipping.
   *
   * @return bool
   *   True if relay shipping, false otherwise.
   */
  public function isRelay(): bool {
    return $this->getColissimoType() == ColissimoType::RELAY;
  }

  /**
   * {@inheritdoc}
   */
  public function createRemoteShipment(ShipmentInterface $shipment): ?RemoteShipment {
    $label = $this->labels->generate($shipment);
    $shipment->setData('colissimo_label_url', $label->getLabelFile()->getUri());
    $shipment->setTrackingCode($label->getShipment()->getId());
    return $label->getShipment();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteShipment(string $id): ?RemoteShipment {
    return new RemoteShipment($id, $id);
  }

  /**
   * {@inheritdoc}
   */
  public function cancelRemoteShipment(string $id): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteLabelFiles(ShipmentInterface $shipment): array {
    /** @var string $url */
    $url = $shipment->getData('colissimo_label_url');
    return [new RemoteLabelFile($url, $this->t('Colissimo Label'))];
  }

  /**
   * Ignore invalid case on parent method Url namesapce.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipment.
   *
   * @return \Drupal\Core\Url|null
   *   The tracking url.
   */
  public function getTrackingUrl(ShipmentInterface $shipment): ?Url {
    /** @var string $parcelNumber */
    $parcelNumber = $shipment->getTrackingCode();
    if (!$parcelNumber) {
      return NULL;
    }
    return Url::fromUri('https://www.laposte.fr/outils/suivre-vos-envois?code=' . $parcelNumber);
  }

}
