<?php

namespace Drupal\commerce_shipping_colissimo\Plugin\Commerce\InlineForm;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use Drupal\address\FieldHelper;
use Drupal\commerce\CurrentCountryInterface;
use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormBase;
use Drupal\commerce_order\AddressBookInterface;
use Drupal\commerce_order\Plugin\Commerce\InlineForm\CustomerProfile;
use Drupal\commerce_shipping_colissimo\Api\WidgetApi;
use Drupal\commerce_shipping_colissimo\CustomerProfileWrapper;
use Drupal\commerce_shipping_colissimo\MobilePhoneNumber;
use Drupal\commerce_shipping_colissimo\RelayFormHelper;
use Drupal\commerce_shipping_colissimo\Settings;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Error;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides the customer_profile inline form.
 *
 * Display the relay selection widget for colissimo relay methods, otherwise
 * fallsback to classic CustomerProfile behavior.
 */
final class ColissimoRelayProfile extends EntityInlineFormBase {

  /**
   * LoggerChannelInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * WidgetApi.
   *
   * @var \Drupal\commerce_shipping_colissimo\Api\WidgetApi
   */
  protected WidgetApi $widgetApi;

  /**
   * Settings.
   *
   * @var \Drupal\commerce_shipping_colissimo\Settings
   */
  protected Settings $settings;

  /**
   * RelayFormHelper.
   *
   * @var \Drupal\commerce_shipping_colissimo\RelayFormHelper
   */
  protected RelayFormHelper $relayFormHelper;

  /**
   * CustomerProfile.
   *
   * @var \Drupal\commerce_order\Plugin\Commerce\InlineForm\CustomerProfile
   */
  protected CustomerProfile $customerProfile;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\commerce_order\AddressBookInterface $addressBook
   *   The address book.
   * @param \Drupal\commerce\CurrentCountryInterface $currentCountry
   *   The current country.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   * @param \Drupal\commerce_shipping_colissimo\Api\WidgetApi $widgetApi
   *   The widget api.
   * @param \Drupal\commerce_shipping_colissimo\Settings $settings
   *   The settings.
   * @param \Drupal\commerce_shipping_colissimo\RelayFormHelper $relayFormHelper
   *   The relay form helper.
   */
  public function __construct(
    array $configuration,
                               $plugin_id,
                               $plugin_definition,
    AddressBookInterface $addressBook,
    CurrentCountryInterface $currentCountry,
    AccountInterface $currentUser,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelInterface $logger,
    WidgetApi $widgetApi,
    Settings $settings,
    RelayFormHelper $relayFormHelper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->widgetApi = $widgetApi;
    $this->settings = $settings;
    $this->relayFormHelper = $relayFormHelper;
    $this->customerProfile = new CustomerProfile($configuration, $plugin_id, $plugin_definition, $addressBook, $currentCountry, $currentUser, $entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_order.address_book'),
      $container->get('commerce.current_country'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.commerce_shipping_colissimo'),
      $container->get(WidgetApi::class),
      $container->get(Settings::class),
      $container->get(RelayFormHelper::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inlineForm, FormStateInterface $formState) {
    if (!$this->isDisplayRelayForm($formState)) {
      return $this->customerProfile->buildInlineForm($inlineForm, $formState);
    }
    $inlineForm = parent::buildInlineForm($inlineForm, $formState);
    $inlineForm['#theme'] = 'colissimo_relay_profile';

    try {
      $token = $this->widgetApi->getWidgetAuthenticationToken();
    }
    catch (\Exception $e) {
      $this->logger->error(Error::renderExceptionSafe($e));
      $token = NULL;
    }

    if (!$token) {
      $inlineForm['error'] = [
        'message' => [
          '#markup' => $this->t('Colissimo relay is temporarily unavailable. Please try again later.'),
        ],
      ];
      return $inlineForm;
    }

    $profileWrapper = $this->getProfileWrapper();

    $address = $profileWrapper->getAddress() ?: [];
    foreach ($this->getAddressFields() as $field) {
      $inlineForm[$field] = [
        '#type' => 'hidden',
        '#default_value' => $address[$field],
        '#attributes' => [
          'class' => [$field],
        ],
      ];
    }

    $inlineForm['display'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => [
        'class' => ['relay-title'],
      ],
    ];
    $inlineForm['relay_id'] = [
      '#type' => 'hidden',
      '#default_value' => $profileWrapper->getRelayId(),
      '#attributes' => [
        'class' => ['relay_id'],
      ],
    ];
    $inlineForm['colissimo_product_code'] = [
      '#type' => 'hidden',
      '#default_value' => $profileWrapper->getProductCode(),
      '#attributes' => [
        'class' => ['colissimo_product_code'],
      ],
    ];
    $inlineForm['choose_relay'] = [
      '#type' => 'button',
      '#value' => $this->t('Choose Relay'),
      '#attributes' => [
        'class' => ['colissimo-choose-relay-btn'],
      ],
    ];
    $setting = $this->settings->get();
    if (!$setting->isPhoneFromBillingProfile()) {
      $inlineForm['mobile_phone'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Mobile Phone'),
        '#default_value' => $profileWrapper->getMobilePhone($setting),
        '#required' => TRUE,
      ];
    }
    $inlineForm['#attributes'] = [
      'class' => ['colissimo-widget-container'],
    ];
    $inlineForm['#attached'] = [
      'library' => 'commerce_shipping_colissimo/relay_profile',
      'drupalSettings' => [
        'commerce_shipping_colissimo' => [
          'relayProfile' => [
            'token' => $token,
            'baseUrl' => $this->settings->get()->getBaseUrl(),
          ],
        ],
      ],
    ];

    return $inlineForm;
  }

  /**
   * Is display relay form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return bool
   *   True if display relay form, false otherwise.
   */
  protected function isDisplayRelayForm(FormStateInterface $formState) {
    return @$this->configuration['profile_scope'] == 'shipping'
      && $this->relayFormHelper->isColissimoRelay($formState);
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inlineForm, FormStateInterface $formState) {
    if (!$this->isDisplayRelayForm($formState)) {
      return $this->customerProfile->buildInlineForm($inlineForm, $formState);
    }
    parent::validateInlineForm($inlineForm, $formState);
    $triggeringElement = $formState->getTriggeringElement();
    $buttonType = $triggeringElement['#button_type'] ?? '';

    if (!$buttonType == 'primary') {
      return $formState;
    }

    $input = $this->getInlineInput($inlineForm, $formState);
    if (empty($input['relay_id']) || empty($input['colissimo_product_code'])) {
      $formState->setError($inlineForm, $this->t('Please select a colissimo relay.'));
    }
    if ($this->settings->get()->isPhoneFromBillingProfile()) {
      return $formState;
    }
    $mobilePhone = new MobilePhoneNumber($input['mobile_phone']);
    if (!$mobilePhone->isValid()) {
      $formState->setErrorByName('mobile_phone', $this->t('The phone number must be at least 10 digits.'));
    }
    return $formState;
  }

  /**
   * {@inheritdoc}
   */
  public function submitInlineForm(array &$inlineForm, FormStateInterface $formState) {
    $profileWrapper = $this->getProfileWrapper();
    if (!$this->isDisplayRelayForm($formState)) {
      $profileWrapper->setRelayId(NULL);
      $this->customerProfile->submitInlineForm($inlineForm, $formState);
      return;
    }
    parent::submitInlineForm($inlineForm, $formState);
    $input = $this->getInlineInput($inlineForm, $formState);

    $profileWrapper->setRelayId($input['relay_id']);
    $profileWrapper->setProductCode($input['colissimo_product_code']);
    $setting = $this->settings->get();
    if (!$setting->isPhoneFromBillingProfile()) {
      $profileWrapper->setMobilePhone(trim($input['mobile_phone']), $setting);
    }
    $address = [];
    foreach ($this->getAddressFields() as $field) {
      $address[$field] = $input[$field];
    }
    $profileWrapper->setAddress($address);
    $profileWrapper->clearAddressBook();
    $profileWrapper->save();
  }

  /**
   * Get profile wrapper.
   *
   * @return \Drupal\commerce_shipping_colissimo\CustomerProfileWrapper
   *   The profile wrapper.
   */
  protected function getProfileWrapper(): CustomerProfileWrapper {
    $entity = $this->getEntity();
    assert($entity instanceof ProfileInterface);
    return new CustomerProfileWrapper($entity);
  }

  /**
   * Get inline input.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The inline input.
   */
  protected function getInlineInput(array &$form, FormStateInterface $formState): array {
    $userInput = $formState->getUserInput();
    return NestedArray::getValue($userInput, $form['#parents']);
  }

  /**
   * Get address field name.
   *
   * @return string[]
   *   Address fields.
   *
   * @throws \ReflectionException
   */
  protected function getAddressFields() {
    $fields = [];
    foreach (AddressField::getAll() as $field) {
      $fields[] = FieldHelper::getPropertyName($field);
    }
    $fields[] = 'country_code';
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    $this->customerProfile->setEntity($entity);
    return parent::setEntity($entity);
  }

}
