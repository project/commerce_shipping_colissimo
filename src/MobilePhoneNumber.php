<?php

namespace Drupal\commerce_shipping_colissimo;

/**
 * Validate/format phone numbers according to colissimo api rules.
 */
class MobilePhoneNumber {

  /**
   * Number.
   *
   * @var string
   */
  private string $number;

  /**
   * Constructor.
   *
   * @param string|null $number
   *   Number.
   */
  public function __construct(?string $number) {
    $this->number = $number ?: '';
  }

  /**
   * Format.
   *
   * @return string
   *   Formatted number.
   */
  public function format(): string {
    return preg_replace("/[^0-9+]/", '', $this->number);
  }

  /**
   * Is valid.
   *
   * @return bool
   *   True if valid, false otherwise.
   */
  public function isValid() {
    return preg_match('/^\+?[0-9]{10,}$/', $this->format()) === 1;
  }

}
